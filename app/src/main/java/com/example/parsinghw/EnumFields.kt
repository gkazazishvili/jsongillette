package com.example.parsinghw

enum class EnumFields (val type:String){
    ACTIVE("true"),
    TEXT("text"),
    NUMBER("number"),
    INPUT("input"),
    CHOOSER("chooser"),
    REQUIRED("true"),
    GENDER("Gender"),
    BIRTHDAY("Birthday")
}