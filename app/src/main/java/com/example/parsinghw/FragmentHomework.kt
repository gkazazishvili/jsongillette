package com.example.parsinghw

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.parsinghw.databinding.FragmentHomeworkBinding


class FragmentHomework() : BaseFragment<FragmentHomeworkBinding>(FragmentHomeworkBinding::inflate) {

    val viewM: PostsViewModel by viewModels()


//    override var useSharedViewModel = true
//
//    override fun getViewModelClass(): Class<PostsViewModel> = PostsViewModel::class.java


    override fun start() {
        fetchData()
    }

    private fun fetchData() {


        viewM.parseJson()
        viewM.parsedJson.observe(viewLifecycleOwner, {


            binding.recyclerView.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            binding.recyclerView.adapter = Adapter(viewM.parsedJson)

        })

    }


}






