package com.example.parsinghw

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


open class Adapter(private val list: LiveData<FieldsData>) :
    RecyclerView.Adapter<Adapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_parent, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {


        holder.recyclerView.layoutManager = LinearLayoutManager(holder.recyclerView.context)
        holder.recyclerView.adapter = ChildAdapter(list.value?.get(position))
    }

    override fun getItemCount(): Int {
        return list.value?.size!!
    }

  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

      var recyclerView: RecyclerView = itemView.findViewById(R.id.second_recyclerview)


    }




}


